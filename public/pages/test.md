---
title: Test
layout: page
---

# Заголовок первого уровня

## Заголовок второго уровня

### Заголовок третьего уровня

#### Заголовок четвертого уровня

##### Заголовок пятого уровня

###### Заголовок шестого уровня

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum sed, dicta nobis quisquam exercitationem? Nemo quas delectus, a aspernatur excepturi recusandae nihil mollitia itaque modi doloremque. Officiis non, iure natus.

С ростом сложности веб-приложений усложняется задача обновления и вывода их базовых данных. Многие подходы по управлению этими данными приводят к сложной паутине представлений. Эти представления могут прослушивать обновления различных моделей, отправляющих свои изменения еще большему количеству представлений.

Все это оставляет разработчиков с непрозрачным и непредсказуемым кодом, который практически невозможно изменить, не забыв при этом прикрепить прослушиватель к какому-нибудь важному элементу. Еще хуже, что при этом разработчик может добавить баг в другом, на первый взгляд, совершенно не связанном участке приложения.

    <button onclick="dispatch(addTodo('Walk dog'))">Add Walk Dog Todo</button>

    <script>
      // Redux setup code would go here
      let nextTodoId = 0;
      const addTodo = (task) => {
        return {
          type: 'ADD_TODO',
          id: nextTodoId++,
          task
        };
      };
    </script>

* [Smashing Magazine](https://www.smashingmagazine.com)
* [TechCrunch](http://techcrunch.com/)
* [Ars Technica](http://arstechnica.com/)
* [Codoki](http://codoki.com/)
* [Mozilla Javascipt](https://developer.mozilla.org/en-US/docs/Web/JavaScript)