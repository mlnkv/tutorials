Site({
  navItems: [
    {
      name: 'About'
    },
    {
      name: 'Gitlab',
      href: 'https://gitlab.com/mlnkv/tutorials/tree/master/public',
      target: '_blank'
    }
  ],
  attrributes: {
    sitename: 'Site name',
    tagline: 'Site tagline',
    'footer-text': '&copy; ' + new Date().getFullYear() + ' All Rights Reserved.'
  }
})

var monthes = 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec'.split(',')

Site.formatters.date = function(time) {
  date = new Date(time)
  var d = date.getDate().toString()
  return (d[1] ? d : '0' + d) + ' ' + monthes[date.getMonth()]  + ' ' + date.getFullYear()
}
Site.formatters.markdown = function(string) {
  return marked(string)
}

Site.formatters.slugify = function slugify(text) {
  return text.toString()
    .toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^\w\-]+/g, '')
    .replace(/\-\-+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '');
}

Site.on('rendered', function(type) {
  if (type === 'post') {
    for (var i = 0, nodes = document.body.querySelectorAll('pre code'); i < nodes.length; i++) {
      hljs.highlightBlock(nodes[i]);
    }
  }
})