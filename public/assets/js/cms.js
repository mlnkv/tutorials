(function(global) {

  var settings = {
    navItems: [],
    attrributes: [],
    content: {
      posts: 'posts',
      pages: 'pages'
    },
    mode: 'apache',
    parseSeperator: '---'
  },
  routes = {
    '': renderPosts,
    post: renderPost,
    page: renderPage
  },
  siteContent = {},
  callbacks = {},
  bodyClass

  function Site(options) {
    each(options || {}, function(item, prop) {
      settings[prop] = item
    })
    setAttributes()
    getContent(function() {
      render()
      addNavigation()
      if (settings.ready) {
        settings.ready()
      }
    })
    window.addEventListener('hashchange', render)
  }

  function addNavigation() {
    var nav = $('.cms-nav')
    settings.navItems.forEach(function(item) {
      each(nav, function(el) {
        var link = mk('a')
        if (item.href) {
          link.href = item.href
          if (item.target) {
            link.target = item.target
          }
        } else {
          var page = siteContent.pages.find(function(page) {
            return page.title == item.name
          })
          if (page) {
            link.href = '#page/' + page.slug
          }
        }
        link.textContent = item.name
        if (link.href) {
          el.appendChild(link)
        }
      })
    })
  }

  function setAttributes() {
    each(settings.attrributes, function(text, query) {
      each($('.cms-' + query), function(el) {
        el.innerHTML = text
      })
    })
  }

  function getContent(callback) {
    var counter = 0
    each(settings.content, function(folder, name) {
      siteContent[name] = []
      get(folder + '/', function(response) {
        var links
        if (settings.mode == 'github') {
          links = JSON.parse(response)
        } else {
          var parent = document.createElement('div')
          parent.innerHTML = response
          links = $('a', parent)
        }
        each(links, function(link) {
          var file = {}, filename, download_url
          if (settings.mode == 'github') {
            filename = link.name
            download_url = link.download_url
          } else {
            filename = link.getAttribute('href')
          }
          if (filename.endsWith('.md')) {
            var date = filename.substr(0, 10)
            if (date = Date.parse(date)) file.date = date
            file.name = filename
            file.slug = (file.date ? filename.substr(11) : filename).slice(0, -3)
            if (download_url) file.link = download_url
            counter++
            get(file.link || folder + '/' + filename, function(response) {
              response = response.split(settings.parseSeperator)
              each(response[1].split('\n'), function(str) {
                if (str = str.trim()) {
                  str = str.split(':')
                  file[str[0]] = str[1].trim()
                }
              })
              response.splice(0, 2)
              file.text = response.join('---')
              file.text = file.text.split('<!--more-->')
              if (file.text[1]) {
                file.exerpt = file.text[0]
                file.text = file.text[1]
              } else {
                file.text = file.text[0]
              }
              siteContent[name].push(file)
              counter--
              if (counter === 0) callback()
            })
          }
        })
      })
    })
  }

  function render() {
    var params = location.hash.slice(1).split('/')
    if (routes[params[0]]) {
      routes[params[0]].call(this, params[1])
    } else {
      // error('Error loading page. [' + type + ']')
    }
    scrollTo(0, 0)
  }

  function renderPosts() {
    var container = $('.cms-content')[0]
    container.innerHTML = ''
    siteContent.posts.sort(function(a, b) {
      return a.date - b.date
    }).reverse()
    each(siteContent.posts, function(post) {
      container.innerHTML += renderTemplate('posts-list-item', post)
    })
    if (bodyClass) {
      document.body.classList.remove(bodyClass)
    }
    Site.fire('rendered', 'posts')
  }

  function renderPost(slug) {
    var container
    var post = siteContent.posts.find(function(post) {
      return post.slug == slug
    })
    if (post['outlet']) {
      container = $(post['outlet'])[0]
    } else {
      container = $('.cms-content')[0]
    }
    container.innerHTML = ''
    if (post) {
      container.innerHTML = renderTemplate('post-single', post)
      if (bodyClass) {
        document.body.classList.remove(bodyClass)
      }
      if (post['body-class']) {
        document.body.classList.add(post['body-class'])
        bodyClass = post['body-class']
      }
      Site.fire('rendered', 'post')
    } else {
      // error()
    }
  }

  function renderPage(slug) {
    var container
    var page = siteContent.pages.find(function(page) {
      return page.slug == slug
    })
    if (page['outlet']) {
      container = $(page['outlet'])[0]
    } else {
      container = $('.cms-content')[0]
    }
    container.innerHTML = ''
    if (page) {
      container.innerHTML = renderTemplate('page-single', page)
      if (bodyClass) {
        document.body.classList.remove(bodyClass)
      }
      if (page['body-class']) {
        document.body.classList.add(page['body-class'])
        bodyClass = page['body-class']
      }
      Site.fire('rendered', 'page')
    } else {
      // error()
    }
  }

  function $(q, c) {
    return (c || document).querySelectorAll(q)
  }

  function mk(n) {
    return document.createElement(n)
  }

  function each(source, iterator) {
    if (source.length)
      for (var i = 0, ln = source.length; i < ln; i++) iterator(source[i], i)
    else
      for (var i in source) iterator(source[i], i)
  }

  function get(url, cb) {
    var xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function() {
      if (xhr.status >= 200 && xhr.status < 400) cb(xhr.responseText)
    }
    xhr.send()
  }
  var templatesCache = {}
  var templateReplacer = /{\s*(.+?)(?:\s*\|\s*(.+?))*\s*}/g
  function renderTemplate(name, data) {
    if (!templatesCache[name]) {
      templatesCache[name] = $('#' + name)[0].innerHTML
    }
    return templatesCache[name].replace(templateReplacer, function(str, prop, filter) {
      var result = data[prop] != null ? data[prop] : ''
      if (filter && Site.formatters[filter])
        return Site.formatters[filter](result)
      return result
    })
  }

  Site.formatters = {}

  Site.on = function(name, callback) {
    if (!callbacks[name]) callbacks[name] = []
    callbacks[name].push(callback)
  }
  Site.fire = function(name) {
    var args = [].slice.call(arguments, 1)
    if (callbacks[name]) {
      callbacks[name].map(function(cb) {
        cb.apply(Site, args)
      })
    }
  }

  global.Site = Site
})(window)