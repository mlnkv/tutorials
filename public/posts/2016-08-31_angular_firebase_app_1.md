---
title: Создание приложения на AngularJS и Firebase. Часть I.
category: JavaScript
---

В данной серии уроков мы задействуем AngularJS и Firebase в создании простого веб приложения. Это будет простая блог-платформа, в которой каждый сможет зарегистрироваться и опубликовать свой пост.

<!--more-->

В данном уроке вам потребуются начальные знания AngularJS, но смысл статьи можно уловить и на ходу.

## Что такое AngularJS?

AngularJS – один из самых популярных JavaScript фрэймворков, реализующих MVC. С его помощью можно создавать динамический веб-приложения, используя возможности HTML. Использование современных подходов таких как биндинг данных и внедрения зависимостей позволяют выиграть время за счёт компактности кода.

## Что такое Firebase?

Firebase позволяет осуществлять синхронизацию данных в режиме реального времени без нужды в разработке бэк-энда. Для работы с данным сервисом нужно всего-навсего осуществлять запросы к их API.

API Firebase обладает всеми нужными функциями для хранения и синхронизации данными в режиме реального времени. Комбинирование возможностей AngularJS и Firebase позволяет добиться внушительных результатов.

## Начало работы с AngularJS

Для начала работы с AngularJS можно воспользоваться проектом `angular-seed`. [Скачать](https://github.com/angular/angular-seed) его можно из репозитория.

```bash
$ git clone https://github.com/angular/angular-seed.git
```

Перейдите в каталог проекта и установите все необходимые зависимости.

```bash
$ cd angular-seed
$ npm install ## Установка зависимостей
```

Запускаем node сервер

```bash
npm start ## Запуск сервера
```

Отправляемся по адресу [http://localhost:8000/app/index.html]( http://localhost:8000/app/index.html) и видим отображение стандартного проекта.

Откройте каталог проекта `angular-seed`. Тут будет располагаться весь код проекта. Внутри каталога app вы сможете найти файл `app.js`, который является основным файлом приложения. Внутри данного файла мы определим основные модули и маршруты. По умолчанию в проекте `angular-seed` есть два представления: `view1` и `view2`. Удалите каталоги `view1` и `view2` из папки `app`.

Давайте начнём всё с нуля. Открываем файл `app.js` и удаляем весь код. В файле `app.js` будут определены маршруты нашего приложения. К примеру, как действовать приложению при переходе по адресу `/home`. Для определения маршрутов нам понадобится AngularJS модуль под названием `ngRoute`. Для подключения данного модуля следует написать следующий код:

    angular.module('myApp', [
      'ngRoute'
    ])

В модуле ngRoute существует специальная переменная `$routeProvider` которую следует использовать для конфигурации маршрутов. Мы передадим `$routeProvider` в метод конфигурации модуля `angular.module` и определим наши маршруты в функциях обратного действия, как показано ниже:

    'use strict';
    angular
      .module('myApp', ['ngRoute'])
      .config(['$routeProvider', function($routeProvider) {
        // Routes will be here
      }]);

Далее открываем файл `index.html` и удаляем всё связанные с `view1` и `view2`. Так же удаляем всё из тела документа, кроме подключения библиотек и div-а с настройками `ngView`.

    <div ng-view></div>

`ngView` является компонентом с помощью которого будет осуществляться рендеринг представления в зависимости от маршрута в `index.html`. Таким образом каждый раз при смене маршрута будет так же меняться и содержание div-а.

Теперь давайте создадим представление (страничку) для входя пользователя в систему. Внутри каталога `app` создайте папку `home`. Внутри создайте два файла: `home.html` и `home.js`. Откройте файл `home.html` и вставьте следующий HTML код:

    <!DOCTYPE html>
    <html lang="en" ng-app="myApp">
      <head>
        <meta charset="utf-8">
        <link rel="icon" href="http://getbootstrap.com/favicon.ico">
        <title>AngularJS & Firebase Web App</title>
        <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="http://getbootstrap.com/examples/signin/signin.css" rel="stylesheet">
        <link href="justified-nav.css" rel="stylesheet">
      </head>
      <body>
        <div class="container">
          <div class="jumbotron" style="padding-bottom:0">
            <h2>AngularJS & Firebase App!</h2>
          </div>
          <form class="form-signin">
            <input type="email" class="form-control" placeholder="Email address" required autofocus>
            <input type="password" class="form-control" placeholder="Password" required>
            <label class="checkbox">
              <a href="#"> Sign Up</>
            </label>
            <button class="btn btn-lg btn-primary btn-block">Sign in</button>
          </form>
        </div>
      </body>
    </html>

Как вы успели заметить, в нашей разметке используется Bootstrap.

Внутри `home.js`, мы определим маршрут при навигации по которому будет открывать представление `home`. В объекте `$routeProvider` есть метод под названием `when`, который мы задействуем в создании маршрута. При определении нового маршрута мы зададим значение `templateUrl`, которое будет отражено в `index.html`. В то же время, мы создадим контроллер для новоиспечённой области видимости (`$scope`) нашего представления `home`. В контроллер пишется логика в зависимости от которой отображается то или иное представление. Вот так оно должно выглядеть:

    'use strict';
    angular
      .module('myApp.home', ['ngRoute'])
      // Определение маршрута
      .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/home', {
          templateUrl: 'home/home.html',
          controller: 'HomeCtrl'
        });
      }])
      // контроллер home
      .controller('HomeCtrl', [function() {
        // ...
      }]);


Теперь открываем `app.js` и подключаем модуль `myApp.home`. Так же определяем какой маршрут будет использоваться по умолчанию с помощью `$routeProvider.otherwise`.

    'use strict';
    angular
      .module('myApp', [
        'ngRoute',
        'myApp.home' // подключаем новый модуль
      ])
      .config(['$routeProvider', function($routeProvider) {
        // назначаем представление по умолчанию
        $routeProvider.otherwise({
          redirectTo: '/home'
        });
      }]);

Далее, для того чтобы отобразит главную страницу нам нужно подключить `home.js` внутри основного HTML шаблона нашего приложения. Открываем `index.html` и вставляем следующий код:

    <script src="home/home.js"></script>

Перезапускаем сервер, отправляемся по адресу [http://localhost:8000/app/index.html](http://localhost:8000/app/index.html) и видим следующее:

![](http://ruseller.com/lessons/les2303/images/home.png)

## Начало работы с Firebase

Для начала работы с [Firebase](https://www.firebase.com/) необходимо зарегистрироваться в данном сервисе. После этого вы получите следующие данные:

![](http://ruseller.com/lessons/les2303/images/fire.png)

Обратите внимание на URL, который был создан для приложения. Далее кликаем на кнопку Manage App. Именно данный URL будет воздействован для работы с Firebase.

Для аутентификации в Firebase будет использоваться email и пароль. Чтобы данный способ сработал, нужно активировать соответствующую опцию в Firebase. Кликнете по вкладке Login & Auth. В открывшемся интерфейсе Email & Password, активируйте настройку Enable Email & Password Authentication:

![](http://ruseller.com/lessons/les2303/images/xpo.png)

Добавьте email пользователя и пароль, которые будут участвовать в процессе аутентификации.

![](http://ruseller.com/lessons/les2303/images/add.png)

## Реализация входа пользователя в систему

Для взаимодействия с Firebase, вставьте следующие подключения скриптов в файл `app/index.html`:

    <script src="https://cdn.firebase.com/js/client/1.0.18/firebase.js"></script>
    <script src="https://cdn.firebase.com/libs/angularfire/0.8.0/angularfire.min.js"></script>
    <script src="https://cdn.firebase.com/js/simple-login/1.6.2/firebase-simple-login.js"></script>

Далее нам необходимо подключить модуль firebase в файле `home.js`:

    angular.module('myApp.home', ['ngRoute', 'firebase'])

Теперь мы готовы осуществить совместную работу с Firebase. Открываем `home.js` и внутри `HomeCtrl`, создаём новую функцию `SignIn` для авторизации пользователя. Для создания новой функции воспользуемся объектом `$scope`. Данный объект относится к модели приложения, и является связующим звеном между контроллером и представлением. Поэтому мы помещаем объект `$scope` в функцию `SignIn`, чтобы иметь доступ к представлению.

    $scope.SignIn = function($scope) {
      var username = $scope.user.email;
      var password = $scope.user.password;
      // логика авторизации
    }

Далее внутри `HomeCtrl` создаём объект для взаимодействия с Firebase:

    var firebaseObj = new Firebase("https://blistering-heat-2473.firebaseio.com");

Модуль `$firebaseSimpleLogin` используется для авторизации в сервисе Firebase, используя `email`, `ID` и пароля. Передаём объект `$firebaseSimpleLogi`n в `HomeCtrl`:

    .controller('HomeCtrl', ['$scope','$firebaseSimpleLogin',function($scope,$firebaseSimpleLogin) {

Используя `firebaseObj` создаём объект `$firebaseSimpleLogin`:

    var loginObj = $firebaseSimpleLogin(firebaseObj);

Теперь, используя [$login API](https://www.firebase.com/docs/web/libraries/angular/api.html#angularfire-firebasesimplelogin-login-provider-options), можем авторизоваться в Firebase. `loginObj.$login` принимает в качестве параметров email и пароль. При успешной проверке будет вызван один колбэк. В обратном случае другой.

    $scope.SignIn = function(event) {
      // предотвращаем перезагрузку страницы
      event.preventDefault();
      var username = $scope.user.email;
      var password = $scope.user.password;
      loginObj
        .$login('password', {
          email: username,
          password: password
        })
        .then(function(user) {
          // колбэк запустится при успешной аутентификации аутентификацииSuccess callback
          console.log('Authentication successful');
        }, function(error) {
          // колбэк при неудаче
          console.log('Authentication failure');
        });
    }

Для полной реализации работы контроллера, необходимо связать его с представлением. Для этого в AngularJS есть специальная функция `ngController`. Открываем `home.html` и добавляем `ngController` в тело документа.

Для аутентификации пользователя нам необходимо передать имя пользователя и пароль в метод `SignIn`. Для биндинга (связки) данных в AngularJS есть опция `ngModel`. Используя её мы сможем передать данные в контроллер:

    <body ng-controller="HomeCtrl">
      <div class="container">
        <div class="jumbotron" style="padding-bottom:0">
          <h2>AngularJS & Firebase App!</h2>
        </div>
        <form class="form-signin" role="form">
          <input ng-model="user.email" type="email" class="form-control" placeholder="Email address" required autofocus>
          <input ng-model="user.password" type="password" class="form-control" placeholder="Password" required>
          <label class="checkbox">
            <a href="#"> Sign Up</>
          </label>
          <button type="button" class="btn btn-lg btn-primary btn-block">SignIn</button>
        </form>
      </div>
    </body>

И наконец прикрепляем `ngClick` к кнопке авторизации:

    <button type="button" ng-click="SignIn($event)" class="btn btn-lg btn-primary btn-block">SignIn</button>

Сохраните все изменения и перезапустите сервер. Зайдите по адресу [http://localhost:8000/app/index.html#/home](http://localhost:8000/app/index.html#/home) и попробуйте авторизоваться в помощью ID jay3dec@gmail.com и пароля jay. При успешной аутентификации в консоли должно отпечататься сообщение Authentication successful.

## Заключение

В этом уроке мы показали как можно начать работать с **AngularJS**. Был создан функционал для аутентификации пользователя, где логин и пароль хранятся в удалённой базе сервиса **Firebase**.

Во второй части мы пойдём дальше и расширим наш вход в систему, используя валидаторы, авторизацию и другие фишки. Весь код проекта можно найти на **GitHub**.

***

Данный урок подготовлен для вас командой сайта [ruseller.com](http://ruseller.com/)
<br>
Источник урока: [tutsplus.com](http://code.tutsplus.com/tutorials/creating-a-web-app-from-scratch-using-angularjs-and-firebase--cms-22391)
<br>
Перевел: Станислав Протасевич
